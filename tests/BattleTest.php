<?php
namespace tests;

use PHPUnit\Framework\TestCase;
use Battle;

require_once 'src/autoload.php';


/**
 * @covers Battle
 */
class BattleTest extends TestCase
{

	protected $fixture;
  const TIMES = 1000;

  protected function setUp() : void
  {
    $this->fixture = new Battle();
  }


  protected function tearDown() : void
  {
    $this->fixture = NULL;
  }


  /**
   * test orderus luck probability
   * @return [type] [description]
   */
  public function testOrderusLuckyBool()
  {

    $cnt = 0;
    $this->fixture->orderus->luck = rand(10,30);

    for($i=0;$i<=self::TIMES;$i++){
      $luck = $this->fixture->is_lucky($this->fixture->orderus);
      if($luck == true) $cnt++;
    }

    $islucky = round($cnt/10);

    $this->assertTrue( $islucky <= $this->fixture->orderus->luck + 3 );

    $this->assertTrue( $islucky >= $this->fixture->orderus->luck - 3 );

  }

  /**
   * test beast luck probability
   * @return [type] [description]
   */
  public function testBeastLuckyBool()
  {

    $cnt = 0;
    $this->fixture->wildbeast->luck = rand(25,40);

    for($i=0;$i<=self::TIMES;$i++){
      $luck = $this->fixture->is_lucky($this->fixture->wildbeast);
      if($luck == true) $cnt++;
    }

    $islucky = round($cnt/10);

    $this->assertTrue( $islucky <= $this->fixture->wildbeast->luck + 3 );

    $this->assertTrue( $islucky >= $this->fixture->wildbeast->luck - 3 );

  }

  /**
   * test orderus rapid strike probability
   * @return [type] [description]
   */
  public function testOrderusRapidStrike()
  {

    $cnt = 0;
    $this->fixture->orderus->rapid_strike = 10;

    for($i=0;$i<=self::TIMES;$i++){
      $rapid = $this->fixture->has_rapid_strike($this->fixture->orderus);
      if($rapid == true) $cnt++;
    }

    $rapidstrike = round($cnt/10);

    $this->assertTrue( $rapidstrike <= $this->fixture->orderus->rapid_strike + 2 );

    $this->assertTrue( $rapidstrike >= $this->fixture->orderus->rapid_strike - 2 );

  }

  /**
   * test orderus magic shield probability
   * @return [type] [description]
   */
  public function testOrderusMagicShield()
  {

    $cnt = 0;
    $this->fixture->orderus->magic_shield = 20;

    for($i=0;$i<=self::TIMES;$i++){
      $shield = $this->fixture->has_magic_shield($this->fixture->orderus);
      if($shield == true) $cnt++;
    }

    $magicshield = round($cnt/10);

    $this->assertTrue( $magicshield >= ($this->fixture->orderus->magic_shield - 3) );

    $this->assertTrue( $magicshield <= ($this->fixture->orderus->magic_shield + 3) );

  }

  /**
   * @dataProvider providerHealth
   */
  public function testMultipleHealthBelowZeroOrderus($health)
  {

    $this->fixture->wildbeast->set_health($health);

    $this->fixture->strike($this->fixture->orderus, $this->fixture->wildbeast);

    $this->assertGreaterThanOrEqual(0,$this->fixture->wildbeast-> get_health());

  }
  public function providerHealth(){
    return [
        [100],
        [30],
        [5],
        [0]
    ];
  }

  /**
   * @dataProvider providerHealthOrderus
   */
  public function testMultipleHealthBelowZeroBeast($health)
  {

    $this->fixture->orderus->set_health($health);

    $this->fixture->strike($this->fixture->wildbeast, $this->fixture->orderus);

    $this->assertGreaterThanOrEqual(0,$this->fixture->orderus-> get_health());

  }
  public function providerHealthOrderus(){
    return [
        [100],
        [30],
        [5],
        [0]
    ];
  }

  //test orderus gets 50% damage during 100 strikes at least once
  public function testMagicSheildHalfDamage(){

    $cnt = 0;
    $this->fixture->orderus->magic_shield = 20;
    for($i=0;$i<=self::TIMES;$i++){

      $this->fixture->strike($this->fixture->wildbeast, $this->fixture->orderus);
      //damage bigger / damage smaller
      if( in_array($i,array(0,1,2)) )
      {
        if( $this->fixture->damage > 0 )
          $damagemin = $damagemax = $this->fixture->damage;
      }
      else
      {
        if($this->fixture->damage > $damagemax) $damagemax = $this->fixture->damage;
        if( $this->fixture->damage < $damagemin && $this->fixture->damage > 0 ) $damagemin = $this->fixture->damage;
      }

    }

    $this->assertTrue( $damagemin == round($damagemax/2) );

  }

  //orderus gets lucky and skips any damage
  public function testLuckySkipDamage(){

    $cnt = 0;
    for($i=0;$i<=self::TIMES;$i++){

      $this->fixture->strike($this->fixture->wildbeast, $this->fixture->orderus);

      if( $this->fixture->damage == 0 ) $cnt++;

    }

    $this->assertGreaterThan(0,$cnt);

  }

}