<?php
namespace tests;

use PHPUnit\Framework\TestCase;
use Character;

require_once 'src/autoload.php';


/**
 * @covers Character
 */
class CharacterTest extends TestCase
{

  public function testSetHealth(){
    $model = new Character();
    $health = rand(60,90);
    $this->assertNull( $model->set_health($health) );
  }

  public function testGetHealth(){
    $model = new Character();
    $this->assertNull($model->get_health());
  }


}