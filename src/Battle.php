<?php

//class running battle methods

class Battle{

	const ROUNDS = 20;
	public $damage = 0;
	public $lucky_turn = false;
	public $rapid_strike_turn = false;
	public $magic_shield_turn = false;
	public $rapid_strike = false;
	public $output_data = '';

	/**
	 * define characters before they start battle
	 * @param array $ch1 pass character 1 data: name,health,strength,defence,speed,luck,rapid_strike,magic_shield
	 * @param array $ch2 pass character 2 data: name,health,strength,defence,speed,luck,rapid_strike,magic_shield
	 */
	public function __construct($ch1 = array(
	'name' => 'character','health' => 80,'strength' => 80,
	'defence' => 50,'speed' => 50,'luck' => 20,'rapid_strike' => 0,'magic_shield' => 0), $ch2 = array('name' => 'character','health' => 80,'strength' => 80,
	'defence' => 50,'speed' => 50,'luck' => 20,'rapid_strike' => 0,'magic_shield' => 0))
	{

		//Orderus
		$this->orderus = new Character($ch1['name'], $ch1['strength'],$ch1['defence'],$ch1['speed'],$ch1['luck'],$ch1['rapid_strike'],$ch1['magic_shield']);
		$this->orderus->set_health($ch1['health']);

		//WhildBeast
		$this->wildbeast = new Character($ch2['name'], $ch2['strength'],$ch2['defence'],$ch2['speed'],$ch2['luck'],$ch2['rapid_strike'],$ch2['magic_shield']);
		$this->wildbeast->set_health($ch2['health']);
		
	}


	/**
	 * initiate and run battle
	 * @return bool battle stops on achieveng one of multiple conditions
	 */
	public function init_battle():void
	{

		//output data before starting the battle
		$this->output_initial_data();

		//run battle process
		$this->battle_process();
		
		//output winner or no winner
		if( $this->orderus->get_health() == 0 || $this->wildbeast->get_health() == 0 ){
			$this->output_winner_data();
		}else{
			$this->output_no_winner();
		}

	}

	//run battle
	public function battle_process():bool
	{

		//battle lifecycle
		for($i = 1; $i <= self::ROUNDS; $i++){

			//select firtst attacker, then switch them
			if( $i == 1 ){
	   		list($attacker,$defender) = $this->select_first_attacker_defender();
			}elseif( !$this->rapid_strike_turn ){
				$tmp = $attacker;
				$attacker = $defender;
				$defender = $tmp;
			}
			
			//attack!
			$this->strike($attacker, $defender);
			//one more attack if has rapid strike
			$this->has_rapid_strike($attacker);

			//output round results
			$this->output_round_results($attacker, $defender, $i);

			//game end condition: zero health
			if( $this->orderus->get_health() == 0 || $this->wildbeast->get_health() == 0 ){
				return true;
			}

		}

		return true;

	}

	/**
	 * strike logic
	 * @param  object $attacker attacker object
	 * @param  object $defender defender object
	 * @return [type]           [description]
	 */
	public function strike($attacker, $defender):void
	{

			//attack action / check if defender is lucky
			$this->damage = $this->is_lucky($defender) ? 0 : $attacker->strength - $defender->defence;

			//apply magic shield skill
			$this->damage = $this->has_magic_shield($defender) ? round($this->damage/2) : $this->damage;

			$health = $defender->get_health() - $this->damage > 0 ? $defender->get_health() - $this->damage : 0;

			$defender->set_health($health);

	}

	/**
	 * check the defender is lucky this turn
	 * @param  object  $defender defender object
	 * @return boolean           returns true if lucky
	 */
	public function is_lucky($defender):bool
	{
		$this->lucky_turn = $defender->luck >=rand(1,100) ? true : false;
		return $this->lucky_turn;
	}

	/**
	 * check attackaer has rapid strike this turn
	 * @param  object  $attacker attacker object
	 * @return boolean           returns true if has rapid strike
	 */
	public function has_rapid_strike($attacker):bool
	{
		$this->rapid_strike_turn = $attacker->rapid_strike >= rand(1,100) ? true : false;
		return $this->rapid_strike_turn;
	}

	/**
	 * check if defender has magic sheild this turn
	 * @param  object  $defender defender object
	 * @return boolean           returns true if attacker has magic shield
	 */
	public function has_magic_shield($defender):bool
	{
		$this->magic_shield_turn = $defender->magic_shield >= rand(1,100) ? true : false;
		return $this->magic_shield_turn;
	}

	/**
	 * select first attacker taking in consideration speed and luck
	 * @return array of objects - returns back the chosen attacker and defender objects
	 */
	public function select_first_attacker_defender():array
	{

		$attacker = $this->orderus;
		$defender = $this->wildbeast;
	
		if( $this->orderus->speed < $this->wildbeast->speed ){
			$attacker = $this->wildbeast;
			$defender = $this->orderus;
		}elseif( $this->orderus->speed == $this->wildbeast->speed ){

			if( $this->orderus->luck < $this->wildbeast->luck ){
				$attacker = $this->wildbeast;
				$defender = $this->orderus;
			}

		}

		return array($attacker,$defender);

	}

	/**
	 * output all character properties
	 */
	public function output_initial_data():void
	{

		$this->output_data .= "Battle start!<br/><br/>\n
		<div>".$this->orderus->name."<br/>\n
		Health : ".$this->orderus->get_health()."<br/>\n
		Strength : ".$this->orderus->strength."<br/>\n
		Speed : ".$this->orderus->speed."<br/>\n
		Luck : ".$this->orderus->luck."<br/></div>\n

		<div>".$this->wildbeast->name."<br/>\n
		Health : ".$this->wildbeast->get_health()."<br/>\n
		Strength : ".$this->wildbeast->strength."<br/>\n
		Speed : ".$this->wildbeast->speed."<br/>\n
		Luck : ".$this->wildbeast->luck."<br/></div>\n";
	}

	/**
	 * output round attack results
	 * @param  object  $attacker attacker object
	 * @param  object  $defender defender object
	 * @param  integer $i        current round
	 */
	public function output_round_results($attacker, $defender, $i = 0):void
	{

		$this->output_data .= 
		"<br/>=====================<br/>\n
		Round: $i<br/>\n";
		$action = $attacker->name == $this->orderus->name ? 'Attack' : 'Defence';

		$this->output_data .=
		"<div>".
		$this->orderus->name." : ".$action."<br/>\n
		Health : ".$this->orderus->get_health()."<br/>\n
		</div>\n";

		$luckyt = $this->lucky_turn == true ? 'yes' : 'no';
		$rapidst = $this->rapid_strike_turn == true ? 'yes' : 'no';
		$magicshieldt = $this->magic_shield_turn == true ? 'yes' : 'no';
		$this->output_data .=
		"<div>
		Damage : ".$this->damage."<br/>\n
		Lucky Turn : ".$luckyt."<br/>\n
		Rapid Strike Turn : ".$rapidst."<br/>\n
		Magic Shield Turn : ".$magicshieldt."<br/>\n
		</div>\n";

		$action = $attacker->name == $this->wildbeast->name ? 'Attack' : 'Defence';
		$this->output_data .=
		"<div>".
		$this->wildbeast->name." : ".$action."<br/>\n
		Health : ".$this->wildbeast->get_health()."<br/>\n
		</div><br/>\n";

	}

	/**
	 * out turn by turn battle data
	 * @return [type] [description]
	 */
	public function output_turn_by_turn(){
		echo $this->output_data;
	}

	/**
	 * output winner name.
	 */
	public function output_winner_data():void
	{
		$winner = $this->orderus->get_health() ? $this->orderus->name : $this->wildbeast->name;
		$this->output_data .= "<br/>==================<br/>\n".$winner." Won!<br/><br/>\n";
	}

	/**
	 * NO winner output
	 * @return [type] [description]
	 */
	public function output_no_winner(){
		$this->output_data .= "<br/>==================<br/>\n 
		NO Winner.<br/><br/>\n";
	}

}


?>