<?php

function autoloader($class) {
    include $class . '.php';
}

spl_autoload_register('autoloader');

// Or, using an anonymous function as of PHP 5.3.0
spl_autoload_register(function ($class) {
    include $class . '.php';
});

?>