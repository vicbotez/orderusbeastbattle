<?php

//class describing character properties

class Character{

	//mandatory character properties
	public $name;
	protected $health;
	public $strength;
	public $speed;
	public $luck;

	//optional character properties
	public $rapid_strike;
	public $magic_shield;

	/**
	 * set mandatory properties
	 * @param string  $name         character name
	 * @param integer $strength     character strenth
	 * @param integer $defence      character defence power
	 * @param integer $speed        character speed
	 * @param integer $luck         character luck
	 * @param integer $rapid_strike character rapid strike chance %
	 * @param integer $magic_shield character magic shield chance %
	 */
	public function __construct($name = 'character', $strength = 80, $defence = 50, $speed = 50, $luck = 20, $rapid_strike = 0, $magic_shield = 0){

		//mandatory properties
		$this->name = $name;
		$this->strength = $strength;
		$this->defence = $defence;
		$this->speed = $speed;
		$this->luck = $luck;

		//special properties
		$this->rapid_strike = $rapid_strike;
		$this->magic_shield = $magic_shield;

	}

	/**
	 * set character health
	 * @param integer $health character health
	 */
	public function set_health($health = 80){
		$this->health = $health;
	}

	/**
	 * get character health
	 * @return integer character health
	 */
	public function get_health(){
		return $this->health;
	}

	//strike
	



}

?>