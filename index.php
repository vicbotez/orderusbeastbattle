<?php

ini_set('display_errors', '1');
error_reporting(E_ALL);


include_once "src/autoload.php";


//name,strength,defence,speed,luck,rapid_strike,magic_shield
$orderus = array(
	'name' => 'Orderus',
	'health' => rand(70,100),
	'strength' => rand(70,80),
	'defence' => rand(45,55),
	'speed' => rand(40,50),
	'luck' => rand(10,30),
	'rapid_strike' => 10,
	'magic_shield' => 20
);

$wildbeast = array(
	'name' => 'WildBeast',
	'health' => rand(60,90),
	'strength' => rand(60,90),
	'defence' => rand(40,60),
	'speed' => rand(40,60),
	'luck' => rand(25,40),
	'rapid_strike' => 0,
	'magic_shield' => 0
);

//define characters
$battle = new Battle($orderus, $wildbeast);

//simulate battle
$battle->init_battle();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Battle</title>
	<style type="text/css">
		div{ display: inline-block; width: 200px; }
	</style>
</head>
<body>

<?php
echo $battle->output_data;
?>
	
</body>
</html>